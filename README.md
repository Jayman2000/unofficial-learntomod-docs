# Unofficial LearnToMod Docs

Unofficial documentation for LearnToMod's Blockly programming language. Not made by the creators of LearnToMod.

## Legal

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/Jayman2000/unofficial-learntomod-docs">
    <span property="dct:title">Jason Yundt</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Unofficial LearnToMod Docs</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="US" about="https://gitlab.com/Jayman2000/unofficial-learntomod-docs">
  United States</span>.
</p>

The file COPYING.txt contains a copy of the CC0 legalcode as plain text.
